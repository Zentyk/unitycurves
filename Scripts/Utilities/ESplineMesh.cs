﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

public class ESplineMesh : MonoBehaviour {

	public ECubicBezierPath path;
	public float lineWidth = 0.1f;
	[Range(1, 50)]
	public int divisionsPerCurve = 1;
	private float separationBetweenDivisions { get { return 1f/(float)divisionsPerCurve/(float)	path.curveCount; } }


	List<Vector3> vertices;
	List<int> triangles;
	Node[,] nodes;
	List<Vector2> uvs;

	void Awake(){
		if(path == null){
			path = GetComponent<ECubicBezierPath>();
		}
	}

	void Start(){
		GenerateMesh();
		DisplayMesh();
	}

	void Update(){
		GenerateMesh();
		DisplayMesh();
	}

	

	void GenerateMesh(){
		vertices = new List<Vector3>();
		triangles = new List<int>();
		uvs = new List<Vector2>();


		int nodeRowCount = (path.curveCount * divisionsPerCurve) + 1;

		nodes = new Node[nodeRowCount, 2];
		for(int i = 0; i < nodeRowCount; i++){
			for(int j = 0; j < 2; j++){
				float newi = separationBetweenDivisions * i;
				Vector3 controlPoint = path.GetPointInCurve(newi);
				Vector3 perpControlPoint = path.PerpendicularOfPoint(newi).normalized * (j == 0 ? 1 : -1);
				nodes[i,j] = new Node(controlPoint + perpControlPoint * lineWidth );
			}
		}

		Square[] squares = new Square[nodeRowCount-1];
		for(int i = 0; i < squares.Length; i++){
			squares[i] = new Square(nodes[i+1,	1],
			                        nodes[i+1,	0],
			                        nodes[i,	0],
			                        nodes[i,	1]);
		}

		for (int i = 0; i < squares.Length; i++){
			TriangulateSquare(squares[i]);
		}
	}

	void TriangulateSquare(Square square){
		MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
	}

	void MeshFromPoints(params Node[] points) {
		AssignVertices(points);
		
		if (points.Length >= 3){
			CreateTriangle(points[0], points[1], points[2]);
		}
		if (points.Length >= 4){
			CreateTriangle(points[0], points[2], points[3]);
		}
		if (points.Length >= 5) {
			CreateTriangle(points[0], points[3], points[4]);
		}
		if (points.Length >= 6){
			CreateTriangle(points[0], points[4], points[5]);
		}
	}

	void AssignVertices(Node[] points) {
		for (int i = 0; i < points.Length; i ++) {
			if (points[i].vertexIndex == -1) {
				points[i].vertexIndex = vertices.Count;
				vertices.Add(points[i].position);
				AssignUVs(points[i]);
			}
		}
	}

	void AssignUVs(Node point){
		int vertIndex = point.vertexIndex;
		if(uvs.Count < 4){
			if(vertIndex % 4 == 0) {
				uvs.Add(Vector2.one);
			} else if (vertIndex % 4 == 1) {
				uvs.Add(Vector2.right);
			} else if (vertIndex % 4 == 2) {
				uvs.Add(Vector2.zero);
			} else {
				uvs.Add(Vector2.up);
			}
		}
		else{
			if(vertIndex % 4 == 2) {
				uvs.Add(Vector2.one);
			} else if (vertIndex % 4 == 3) {
				uvs.Add(Vector2.right);
			} else if (vertIndex % 4 == 1) {
				uvs.Add(Vector2.zero);
			} else {
				uvs.Add(Vector2.up);
			}
		}
	}

	void CreateTriangle(Node a, Node b, Node c) {
		triangles.Add(a.vertexIndex);
		triangles.Add(b.vertexIndex);
		triangles.Add(c.vertexIndex);
	}

	void DisplayMesh(){
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		
		mesh.vertices = vertices.ToArray();
		mesh.uv = uvs.ToArray();



		mesh.triangles = triangles.ToArray();
		mesh.RecalculateNormals();
	}


	public class Node {
		public Vector3 position;
		public int vertexIndex = -1;
		
		public Node(Vector3 _pos) {
			position = _pos;
		}
	}

	public class Square {
		
		public Node topLeft, topRight, bottomRight, bottomLeft;
		
		public Square (Node _topLeft, Node _topRight, Node _bottomRight, Node _bottomLeft) {
			topLeft = _topLeft;
			topRight = _topRight;
			bottomRight = _bottomRight;
			bottomLeft = _bottomLeft;
		}
		
	}


}
