﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ECubicBezierPath : MonoBehaviour {

	public List<Vector3> controlPoints = new List<Vector3>();


	public int curveCount { get { return controlPoints != null ? (controlPoints.Count - 1) / 3 : 0; } }
	public int segmentsPerCurve = 30;
	float percentagePerSection { get { return 1f / curveCount; } } 


	
	public Vector3 GetPointInCurveWorldCoordinates(float t){
		return transform.TransformPoint(GetPointInCurve(t));
	}

	public Vector3 GetPointInCurve(float t){
		int currentCurve = GetCurveNumber(t);
		float reminder = t - (percentagePerSection * (currentCurve - 1));
		float newT =  reminder / percentagePerSection;
		if(currentCurve > curveCount){
			currentCurve = curveCount;
			newT = 1f;
		}
		return GetPointInCurve(newT, currentCurve - 1);
	}

	public int GetCurveNumber(float t){
		int count = 0;
		float percentages = 0;
		while(percentages <= t){
			percentages += percentagePerSection;
			count++;
		}
		return count;
	}

	public Vector3 GetPointInCurve(float t, int section){
		int firstControlIndex = section * 3;
		return GetPointInCurve(t, controlPoints[firstControlIndex], controlPoints[firstControlIndex+1], controlPoints[firstControlIndex+2], controlPoints[firstControlIndex+3]);
	}

	public Vector3 GetPointInCurve(float t,
	                             Vector3 point0, Vector3 point1, Vector3 point2, Vector3 point3)
	{
		float u = 1 - t;
		float tSquared = t*t;
		float uSquared = u*u;
		float uCubed = uSquared * u;
		float tCubed = tSquared * t;
		
		Vector3 p = uCubed * point0; 	//first term
		p += 3 * uSquared * t * point1; //second term
		p += 3 * u * tSquared * point2; //third term
		p += tCubed * point3; 			//fourth term
		
		return p;
	}

	public List<Vector3> GetDrawingPoints(){
		List<Vector3> drawingPoints = new List<Vector3>();
		List<Vector3> localPoints = WorldToLocalPoints(controlPoints);
		for(int i = 0; i < localPoints.Count - 3; i+= 3){
			Vector3 p0 = localPoints[i];
			Vector3 p1 = localPoints[i + 1];
			Vector3 p2 = localPoints[i + 2];
			Vector3 p3 = localPoints[i + 3];

			if(i == 0){
				drawingPoints.Add(GetPointInCurve(0f, p0,p1,p2,p3));
			}

			for(int j = 1; j <= segmentsPerCurve; j++){
				float t = j / (float) segmentsPerCurve;
				drawingPoints.Add(GetPointInCurve(t, p0, p1, p2, p3));
			}
		}
		return drawingPoints;
	}


	//TODO: Refactor in a better way
	public Vector3 PerpendicularOfPoint(float t){
		float threshold = 0.05f;
		if(t > 0 + threshold && t < 1f - threshold){
			Vector3 prevPoint = GetPointInCurve(t - threshold);
			Vector3 nextPoint = GetPointInCurve(t + threshold);

			Vector3 newVect = prevPoint - nextPoint;

			return new Vector3(-newVect.y, newVect.x, newVect.z);
		}
		else if(t < threshold){
			Vector3 point = GetPointInCurve(t);
			Vector3 nextPoint = GetPointInCurve(t + threshold);

			Vector3 newVect = point - nextPoint;

			return new Vector3(-newVect.y, newVect.x, newVect.z);

		}
		else {
			Vector3 prevPoint = GetPointInCurve(t - threshold);
			Vector3 point = GetPointInCurve(t);

			Vector3 newVect = prevPoint - point;

			return new Vector3(-newVect.y, newVect.x, newVect.z);
		}
	}



	public List<Vector3> WorldToLocalPoints(List<Vector3> worldPoints){
		List<Vector3> localPoints = new List<Vector3>();
		for(int i = 0; i < worldPoints.Count; i++){
			localPoints.Add(transform.TransformPoint(worldPoints[i]) );
		}
		return localPoints;
	}

	public List<Vector3> LocalToWorldPoints(List<Vector3> localPoints){
		List<Vector3> worldPoints = new List<Vector3>();
		for(int i = 0; i < localPoints.Count; i++){
			worldPoints.Add(transform.InverseTransformPoint(localPoints[i]) );
		}
		return worldPoints;
	}

	public Vector3 GetControlPoint(int index){
		return (index * 3 < controlPoints.Count ? controlPoints[3 * index] : Vector3.zero);
	}


}
